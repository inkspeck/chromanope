$ ->
  chromaNope = ->
    newImageObject = ->
      ogimage = $("#compare-center img")
      image = new Image()
      image.src = ogimage.attr('src')
      image.width = ogimage.width()
      image.height = ogimage.height()
      return image

    simulateVision = (image, scaleX, scaleY, type, next)->
      Color.Vision.Simulate image, scaleX, scaleY,
        type: type,
        amount: 1.0,
        callback: (canvas) ->
          img = "<div class='#{type} img'><h2>#{type}</h2><img class='#{type}' src='#{canvas.toDataURL('image/png',1)}' download='#{type}.png' name='#{type}'/></div>"
          $('#compare-center').append(img)
          unless typeof next == 'undefined'
            next()

    createNopes = ->
      image = newImageObject()
      image.onload = ->
        simulateVision image, image.width, image.height, "Protanope", ->
          image = newImageObject()
          image.onload = ->
            simulateVision image, image.width, image.height, "Deuteranope", ->
              image = newImageObject()
              image.onload = ->
                simulateVision image, image.width, image.height, "Tritanope", ->
                  $('body, input').css('cursor','default')
                  $("#spinner").addClass('hidden')
    createNopes()

  revealShowroom = ->
    $("#welcome").addClass('hidden')
    $("#comparison").removeClass('removed').removeClass('hidden')
    $("#comparison-controls").removeClass('removed').removeClass('hidden')
    $('body').addClass('showroom')

  comparisonClear = ->
    $("#comparison-clear").bind 'click', ->
      $('body').removeClass('showroom')
      $("#welcome").removeClass('hidden')
      $("#compare-center").empty()
      $("#comparison").addClass('hidden').delay(350).addClass('removed')
      $("#comparison-controls").addClass('hidden').delay(350).addClass('removed')
      $('#url-input').val('')
      $('#url-input').attr('placeholder','Enter a URL to Analyze & Compare.')
  comparisonClear()

  hideError = ->
    $("#error").bind 'click', ->
      $("#error-holder").addClass('hidden')
  hideError()

  showError = ->
    $("#error-holder").removeClass('hidden')
    $("#spinner").addClass('hidden')
    $('body, input').css('cursor','default')
    setTimeout ->
      $("#error-holder").addClass('hidden')
    , 5500

  validateURL = (str)->
    urlregex = new RegExp "^((http|https)\:(\/\/)|(file\:\/{2,3}))?(((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))|(((([a-zA-Z0-9]+)(\.)?)+?)(\.)([a-z]{2}|com|org|net|gov|mil|biz|info|mobi|name|aero|jobs|museum))([\/][\/a-zA-Z0-9\.]*)*?([\/]?(([\?][a-zA-Z0-9]+[\=][a-zA-Z0-9\%\(\)]*)([\&][a-zA-Z0-9]+[\=][a-zA-Z0-9\%\(\)]*)*?))?$"
    n = str.indexOf "."
    if str.substring(0, 4) != "http"
      $("#url-input").val "http://" + str
      str = "http://" + str
    if n is -1
      return false
    if urlregex.test str
      return true
    else
      return false

  bindSubmit = ->
    $('#url-form').bind 'submit', (e) ->
      e.preventDefault()
      $("#url-input").blur()
      if validateURL $("#url-input").val()
        $('#error-holder').addClass('hidden')
        $("#comparison").addClass('hidden').delay(350).addClass('removed')
        $("#welcome").removeClass('hidden')
        $("#spinner").removeClass('hidden')
        $("#compare-center").empty()
        $('body, input').css('cursor','wait')
        $.ajax
          type: 'GET',
          url: '/compare',
          data: $(this).serialize(),
          dataType: 'json'
          success: (data) ->
            file = data.filepath
            location = data.finalurl
            img = $("<div class='ogimage img'><h2>Normal Vision</h2><img id='ogimage' src='#{file}.png' width='472'/></div>")
            revealShowroom()
            $("#compare-center").append(img)
            $("#url-input").val(location)
            $("#ogimage").bind 'load', ->
              chromaNope()
              # centerCompare(2)
          error: ->
            showError()
      else
        showError()
  bindSubmit()

  bindSizePickers = ->
    changeNopes = (size) ->
      $('.Protanope, .Deuteranope, .Tritanope').remove()
      $('#comparison img').attr('width', size)
      $('body, input').css('cursor','wait')
      $("#spinner").removeClass('hidden')
      setTimeout ->
        chromaNope()
      , 800
    $('#small').bind 'click', ->
      changeNopes(200)
      # centerCompare(4)
    $('#medium').bind 'click', ->
      changeNopes(472)
      # centerCompare(2)
    $('#large').bind 'click', ->
      changeNopes(960)
      # centerCompare(1)
  bindSizePickers()

  updateCopyright = ->
    date = new Date()
    year = date.getFullYear()
    $("#copyright").html(year)
  updateCopyright()
