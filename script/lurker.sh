#!/bin/bash

echo $'Phantoms Lurking About'
echo $'----------------------'

while true
do
    sleep 1
    phantoms=`ps aux | grep "[p]hantom" | wc -l`
    echo "phantoms --" $phantoms
done