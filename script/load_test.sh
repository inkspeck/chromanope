#!/bin/bash

urls=(
    facebook.com
    youtube.com
    www.boston.com/bigpicture/
    yahoo.com
    live.com
    msn.com
    wikipedia.org
    blogspot.com
    www.baidu.com
    microsoft.com
    www.qq.com
    bing.com
    ask.com
    adobe.com
    www.taobao.com
    twitter.com
    www.youku.com
    www.soso.com
)

# prevent caching
rm public/screengrabs/*.png
rm load_test.out

for url in ${urls[*]}
do
    # make requests in background processes
    curl -s "http://localhost:3000/compare?url=http%3A%2F%2F${url}" >> load_test.out &
done

# show stats
echo $'CURLs\tPhantoms'
echo $'-----\t--------'

curls=`ps aux | grep "[c]url" | wc -l`
while [[ $curls -gt 0 ]]
do
    sleep 1
    curls=`ps aux | grep "[c]url" | wc -l`
    phantoms=`ps aux | grep "[p]hantom" | wc -l`
    echo $curls $'\t' $phantoms
done

echo "responses written to load_test.out"
