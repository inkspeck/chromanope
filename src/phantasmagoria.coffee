phantom = require 'phantom'
async = require 'async'

MaxPhantoms = 3
MaxWait = 35000
fileDir = "./public/screengrabs/"

# If worker does not call our callback before MaxWait,
# we call the original callback with errors.
# Any subsequent calls to our callback will be swallowed

monitor = (callback, worker) ->
  timedOut = false
  timeoutFn = ->
    timedOut = true
    callback(["Execution Timeout"])

  timeoutId = setTimeout timeoutFn, MaxWait

  worker (errors) ->
    unless timedOut
      clearTimeout timeoutId
      callback(errors)

phantomRunner = (task, callback) ->
  monitor callback, (jobComplete) ->
    phantom.create '--local-to-remote-url-access=yes','--web-security=no', (ph) ->
      ph.createPage (page) ->
        errors = []
        page.set 'viewportSize', width: 1024, height: 768
        page.set "onLoadStarted", ->
          console.log "-- Connecting to #{task.url}"
        page.set "onError", (msg, trace) ->
          for item in trace
            errors.push item
          console.log "Exiting on Script Error from URL  -- #{task.url}"
          ph.exit()
          jobComplete(errors)
        page.set "onLoadFinished", (status) ->
          if status is "success"
            console.log "Will be Rendering to #{fileDir}#{task.img}.png -- #{task.url}"
            page.render "#{fileDir}#{task.img}.png", ->
              console.log "Image Created For -- #{task.url}"
              ph.exit()
              jobComplete()
          if status is "fail"
            ph.exit()
            jobComplete(errors)

        page.open task.url


module.exports = async.queue phantomRunner, MaxPhantoms
