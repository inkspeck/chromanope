request = require 'request'
crypto = require 'crypto'
fs = require 'fs'
express = require 'express'
stylus = require 'stylus'
fluidity = require 'fluidity'
assets = require 'connect-assets'
phantasmagoria = require './phantasmagoria'

fileDir = "./public/screengrabs/"
filePath = "/screengrabs/"

app = express()

app.use "/images", express.static './public/images'
app.use "/screengrabs", express.static fileDir
app.set "view engine", "jade"
app.use assets()

createFilename = (url) ->
  hash = crypto.createHash('md5')
  hash.update(url,'utf8')
  return hash.digest('hex')

fileCache = (url) ->
  img = createFilename(url)
  try
    file = fs.lstatSync "#{fileDir}#{img}.png"
    if file.isFile
      expiration = file.mtime.setMinutes(file.mtime.getMinutes() + 10)
      current = new Date()
      if expiration > current
        return true
      else
        return false
  catch error
    return false

app.get '/', (req, res) ->
  res.render 'index'

app.get '/compare', (req, res) ->
  urlsubmit = req.query.url

  resWithFile = (url)->
    img = createFilename(url)
    res.send 200, { filepath: "#{filePath}#{img}", finalurl: "#{url}"}

  errorOut = ->
    console.log "Completely Clean Error Out! No Phantoms Started."
    res.send 500, "ERROR!"

  phantomGo = (url)->
    img = createFilename(url)
    phantasmagoria.push url:url, img:img, (errors)->
      if errors and errors.length
        console.log "Phantoms Failed?"
        res.send 500, "ERROR!"
      else
        res.send 200, { filepath: "#{filePath}#{img}", finalurl: "#{url}"}

  checkURL = (urlsubmit) ->
    options =
      url: urlsubmit

    request.get options, (err, res) ->
      if !err and res.statusCode is 200
        if fileCache(urlsubmit) is true
          console.log "Serving Cached Image UrlSubmit #{urlsubmit}"
          resWithFile(urlsubmit)
        else
          phantomGo(urlsubmit)
      else
        console.log "REQUEST ERROR: " + err
        errorOut()
  checkURL(urlsubmit)

port = process.env.PORT or process.env.VMC_APP_PORT or 3000
app.listen port, ->
  console.log "Listening on #{port}\nPress CTRL-C to stop server."
